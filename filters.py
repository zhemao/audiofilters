import numpy as np
from scikits.audiolab import Sndfile, play
from numpy.fft import fft, ifft

def timeshift(audio, delay=0.2, fs=48000):
    shift = int(delay * fs)
    result = np.roll(audio, shift)
    zeroshape = (shift,) + audio.shape[1:]
    result[:shift] = np.zeros(zeroshape)
    return result

def echo(audio, dropoff=0.5, times=2, delay=0.2, fs=48000):
    totalscale = 1.0
    scale = dropoff
    result = audio

    for i in range(1,times):
        result += scale * timeshift(audio, delay * i)
        totalscale += scale
        scale *= dropoff

    return result / totalscale

def hann2d(shape):
    N = shape[0]
    result = np.zeros(shape)
    for i in range(shape[1]):
        result[:,i] = np.hanning(N)
    return result

def phasewrap(angles):
    for i in range(angles.shape[0]):
        for j in range(angles.shape[1]):
            while angles[i][j] < -np.pi:
                angles[i][j] += 2 * np.pi
            while angles[i][j] > np.pi:
                angles[i][j] -= 2 * np.pi

def resample(audio, scale=2):
    result = np.zeros((audio.shape[0] / scale, audio.shape[1]))

    for i in range(result.shape[0]):
        t = i * scale
        tlow = int(np.floor(t))
        thigh = int(np.ceil(t))
        off = t - tlow
        result[i] = audio[tlow] + (audio[thigh] - audio[tlow]) * off

    return result


def pitchshift(audio, scale=2, fs=48000):
    N = 1024
    H = N / 4
    L = audio.shape[0]
    binshape = (N, audio.shape[1])
    phi = np.zeros(binshape)
    win = hann2d(binshape)
    result = np.zeros((L/scale + N, audio.shape[1]))
    amp = np.max(audio)

    p = 0
    pp = 0

    while p < L - (N + H) and pp < (result.shape[0] - N):
        spec1 = fft(win * audio[p:p+N])
        spec2 = fft(win * audio[p+H:p+N+H])

        phi += (np.angle(spec2) - np.angle(spec1))
        phasewrap(phi)
        fout = np.e ** (1j * phi)
        tout = win * ifft(abs(spec2) * fout)

        p1 = int(pp)
        result[p1:p1+N] += tout.real

        p += H
        pp += H * scale

    result = resample(result)

    return result * amp / np.max(result)

def playfilter(filterfunc, fname, **kwargs):
    f = Sndfile(fname, 'r')
    audio = f.read_frames(f.nframes)
    filtered = filterfunc(audio, fs=f.samplerate, **kwargs)
    play(filtered.transpose())
